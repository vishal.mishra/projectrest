import os
import json

from django.test import Client
from django.conf import settings

from test_plus.test import TestCase
from qa.models import Question, Answer
import random
random.seed(2)


class AnswerListGetTest(TestCase):
    def setUp(self):
        u1 = self.make_user('u1')
        u2 = self.make_user('u2')
        u3 = self.make_user('u3')
        users = [u1, u2, u3]

        for i in range(12):
            user = u1 if i % 2 == 0 else u2
            question = Question.objects.create(
                title=f'question-{i}',
                description=f'question-{i}',
                user=user
            )
            for i in range(3):
                Answer.objects.create(
                    user=users[i],
                    question=question,
                    content=f'answer-{i}',
                    votes=i
                )

    def test_answer_list_get(self):
        client = Client()
        client.login(username='u1', password='password')

        response = client.get('/questions/1/answers')
        import ipdb; ipdb.set_trace()
        self.assertEqual(response.status_code, 200)
        MOCK_DATA_DIR = os.path.join(settings.BASE_DIR, 'mock_data')
        with open(f'{MOCK_DATA_DIR}/answer_list.json', 'r') as f:
            expected_content = f.read()
        expected_content = json.loads(expected_content)
        self.assertEqual(response.json(), expected_content)

'''

{
'count': 3, 
'next': None, 
'previous': None, 
'results': 
[{'user': 1, 'question': 'http://testserver/questions/1', 'content': 'answer-0', 'votes': 0}, 
 {'user': 2, 'question': 'http://testserver/questions/1', 'content': 'answer-1', 'votes': 1},
 {'user': 3, 'question': 'http://testserver/questions/1', 'content': 'answer-2', 'votes': 2}
]
 }



ipdb> response.json()
{
'count': 36, 
'next': 'http://testserver/questions/1/answers?page=2', 
'previous': None, 
'results': 
[
{'user': 1, 'question': 'http://testserver/questions/1', 'content': 'answer-0', 'votes': 0}, 
{'user': 2, 'question': 'http://testserver/questions/2', 'content': 'answer-1', 'votes': 1}, 
{'user': 3, 'question': 'http://testserver/questions/3', 'content': 'answer-2', 'votes': 2}, 
{'user': 1, 'question': 'http://testserver/questions/4', 'content': 'answer-0', 'votes': 0}
]
}
'''